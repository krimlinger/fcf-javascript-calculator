import React from 'react';
import './App.scss';
import { Container } from '../BootstrapGrid';
import Calculator from './Calculator';

export default function App() {
  return (
    <Container
      as="main"
      className="rounded bg-light border p-3"
    >
      <Calculator />
    </Container>
  );
}
