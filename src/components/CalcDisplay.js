import React from 'react';

export default function CalcDisplay({ display }) {
  return (
    <div
      className="rounded bg-dark text-white text-right p-1"
      id="display"
    >
      {display ? display: 0}
    </div>
  );
}
