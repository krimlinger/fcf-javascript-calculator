import React from 'react';
import './CalcPad.scss';
import Button from 'react-bootstrap/Button';

export default class CalcPad extends React.Component {
  _isMounted = false;

  constructor(props) {
    super(props);
    this.state = { active: false };
  }

  componentDidMount() {
    this._isMounted = true;
    document.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyDown);
    this._isMounted = false;
  }

  handleKeyDown = (e) => {
    if (e.key === String(this.props.value) ||
      (this.props.value === 'x' && e.key === '*')) {
      this.handleActivate()
    }
  }

  handleActivate = () => {
      this.setState({ active: true});
      this.props.onClick(this.props.value);
      setTimeout(() => {
        this._isMounted && this.setState({ active: false})
      }, 500);
  }

  render() {
    return (
      <div className="col-padding">
        <Button
          block
          variant={this.props.variant}
          id={this.props.id}
          onClick={this.handleActivate}
          active={this.state.active}
        >
          {this.props.value}
        </Button>
      </div>
    );
  }
}

CalcPad.defaultProps = { variant: 'secondary' };
