import React from 'react';
import './CalcPadContainer.scss';
import { Row, Col } from '../BootstrapGrid';
import OrigCalcPad from './CalcPad';

export default function CalcPadContainer({ onCalcPadClick }) {
  const CalcPad = (props) => {
    return <OrigCalcPad {...props} onClick={onCalcPadClick} />;
  }

  return (
    <>
      <Row noGutters>
        <Col xs={6}>
          <CalcPad id="clear" value="C" variant="danger" />
        </Col>
        <Col xs={3}>
          <CalcPad id="divide" value="/" />
        </Col>
        <Col xs={3}>
          <CalcPad id="multiply" value="x" />
        </Col>
      </Row>
      <Row noGutters>
        <Col>
          <CalcPad id="seven" value={7} />
        </Col>
        <Col>
          <CalcPad id="eight" value={8} />
        </Col>
        <Col>
          <CalcPad id="nine" value={9} />
        </Col>
        <Col>
          <CalcPad id="subtract" value="-" />
        </Col>
      </Row>
      <Row noGutters>
        <Col>
          <CalcPad id="four" value={4} />
        </Col>
        <Col>
          <CalcPad id="five" value={5} />
        </Col>
        <Col>
          <CalcPad id="six" value={6} />
        </Col>
        <Col>
          <CalcPad id="add" value="+" />
        </Col>
      </Row>
      <Row noGutters>
        <Col xs={9}>
          <Row noGutters>
            <Col>
              <CalcPad id="one" value={1} />
            </Col>
            <Col>
              <CalcPad id="two" value={2} />
            </Col>
            <Col>
              <CalcPad id="three" value={3} />
            </Col>
          </Row>
          <Row noGutters>
            <Col>
              <CalcPad id="zero" value={0} />
            </Col>
            <Col>
              <CalcPad id="decimal" value="." />
            </Col>
          </Row>
        </Col>
        <Col xs={3} className="d-flex">
          <CalcPad id="equals" value="=" variant="primary" />
        </Col>
      </Row>
    </>
  );
}
