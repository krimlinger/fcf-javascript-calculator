import React from 'react';
import { Row, Col } from '../BootstrapGrid';
import CalcDisplay from './CalcDisplay';
import CalcPadContainer from './CalcPadContainer';

const OPERATORS = '+-x/'.split('');
const NUMBERS = [...Array(10).keys()].map(String);
const DECIMAL = '.';
const RESET = 'C';
const EQUAL = '=';

function operatorToFunc(op) {
  switch (op) {
    case '+':
      return (a, b) => a + b;
    case '-':
      return (a, b) => a - b;
    case 'x':
      return (a, b) => a * b;
    case '/':
      return (a, b) => a / b;
    default:
      throw Error('Cannot find function for given operator');
  }
}

export default class Calculator extends React.Component {
  constructor(props) {
    super(props);
    this.defaultState = {
      curNumber: '',
      curOperator: null,
      result: null,
      display: '',
      nextShouldBeNumber: false,
    };

    this.state = Object.assign({}, this.defaultState);
  }

  handleCalcPadClick = (value) => {
    this.handleActivate(value);
  }

  computeResult = () => {
    if (!this.state.curNumber
      || !this.state.curOperator
      || !this.state.result) {
      return;
    }
    this.setState((s) => {
      const result = String(operatorToFunc(s.curOperator)(Number(s.result), Number(s.curNumber)));
      return ({
        curNumber: '',
        curOperator: null,
        result: result,
        display: result,
      })
    });
  }

  handleActivate = (value) => {
    const v = String(value);

    if (this.state.nextShouldBeNumber && !NUMBERS.includes(v)) {
      return;
    }

    if (OPERATORS.includes(v)) {
      if (!this.state.curOperator) {
        if (!this.state.result) {
          this.setState((s) => ({
            result: s.curNumber,
            curNumber: '',
            curOperator: v,
          }));
        } else {
          this.setState({ curOperator: v});
          this.computeResult();
        }
      } else {
        if (!this.state.curNumber) {
          if (v === '-') {
            this.setState((s) => ({
              display: s.display + v,
              curNumber: v,
              nextShouldBeNumber: true,
            }));
            return;
          }
          this.setState((s) => ({
            curOperator: v,
            display: s.display.substring(0, s.display.length-1),
          }));
        } else {
          if (!this.state.result) {
            this.setState((s) => ({
              curNumber: '',
              result: s.curNumber,
            }));
          } else {
            this.computeResult();
            this.setState({ curOperator: v});
          }
        }
      }
    } else if (NUMBERS.includes(v)) {
      if (this.state.result && !this.state.curOperator) {
        return;
      }
      this.setState((s) => ({
        curNumber: s.curNumber + v,
        nextShouldBeNumber: false,
      }));
    } else if (v === DECIMAL) {
      if (this.state.curNumber.includes('.')) {
        return;
      }
      this.setState((s) => ({ curNumber: s.curNumber + v }));
    } else if (v === RESET) {
      this.setState(this.defaultState);
      return;
    } else if (v === EQUAL) {
      this.computeResult();
      return
    }
    this.setState((s) => ({ display: s.display + v}));
  }

  render() {
    return (
      <>
        <Row>
          <Col>
            <CalcDisplay display={this.state.display} />
          </Col>
        </Row>
        <Row>
          <Col>
            <CalcPadContainer
              onCalcPadClick={this.handleCalcPadClick}
            />
          </Col>
        </Row>
      </>
    );
  }
}
